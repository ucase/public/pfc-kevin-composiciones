<!--
Copyright (C) 2017 Kevin J. Valle-Gómez

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-->
<bpel:process name="Triangle"
         targetNamespace="http://eclipse.org/bpel/sample"
         suppressJoinFailure="yes"
         xmlns:tns="http://eclipse.org/bpel/sample"
         xmlns:bpel="http://docs.oasis-open.org/wsbpel/2.0/process/executable"
         >
         
    <!-- Import the client WSDL -->
	<bpel:import location="TriangleArtifacts.wsdl" namespace="http://eclipse.org/bpel/sample" 
	        importType="http://schemas.xmlsoap.org/wsdl/" />

    <!-- ================================================================= -->         
    <!-- ORCHESTRATION LOGIC                                               -->
    <!-- Set of activities coordinating the flow of messages across the    -->
    <!-- services integrated within this business process                  -->
    <!-- ================================================================= -->         
    <bpel:partnerLinks>
        <bpel:partnerLink name="client" partnerLinkType="tns:Triangle" myRole="TriangleResource"></bpel:partnerLink>
    </bpel:partnerLinks>
    <bpel:variables>
        <bpel:variable name="input" messageType="tns:processRequest"></bpel:variable>
        
        <bpel:variable name="output" messageType="tns:processResponse">
        </bpel:variable>
    </bpel:variables>
    <bpel:sequence name="main">
		
        <bpel:receive name="ReceiveInput" partnerLink="client" operation="process" portType="tns:Triangle" createInstance="yes" variable="input">
        </bpel:receive>
        <bpel:if name="IfAllGreaterThanZero">
            <bpel:condition expressionLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0"><![CDATA[$input.payload/a>0 and $input.payload/b>0 and $input.payload/c>0]]></bpel:condition>
            <bpel:sequence name="ContinueExecution">
                <bpel:if name="IfTriangleCanBeFormed">
                    <bpel:condition expressionLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0"><![CDATA[(($input.payload/a + $input.payload/b) > $input.payload/c) and (($input.payload/a + $input.payload/c) > $input.payload/b)]]></bpel:condition>
                    <bpel:sequence name="CheckType1">
                        <bpel:if name="IfAllEqual">
                            <bpel:condition expressionLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0"><![CDATA[$input.payload/a = $input.payload/b and $input.payload/b = $input.payload/c]]></bpel:condition>
                            <bpel:sequence name="Equilateral">
                                <bpel:assign validate="no" name="AssignEquilateral">
                                    <bpel:copy>
                                        <bpel:from><bpel:literal xml:space="preserve"><tns:TriangleResponse xmlns:tns="http://eclipse.org/bpel/sample" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <response>3</response>
</tns:TriangleResponse>
</bpel:literal></bpel:from>
                                        <bpel:to part="payload" variable="output">
                                        </bpel:to>
                                    </bpel:copy>
                                    
                                    <bpel:copy>
                                        <bpel:from expressionLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                                            <![CDATA[3]]>
                                        </bpel:from>
                                        <bpel:to part="payload" variable="output">
                                            <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                                                <![CDATA[response]]>
                                            </bpel:query>
                                        </bpel:to>
                                    </bpel:copy>
                                </bpel:assign>
                            </bpel:sequence>
                        <bpel:elseif>
                                <bpel:condition expressionLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0"><![CDATA[$input.payload/a = $input.payload/b or $input.payload/a = $input.payload/c or $input.payload/b = $input.payload/c]]></bpel:condition>
                                <bpel:sequence name="Isosceles">
                                    <bpel:assign validate="no" name="AssignIsosceles">
                                        <bpel:copy>
                                            <bpel:from><bpel:literal xml:space="preserve"><tns:TriangleResponse xmlns:tns="http://eclipse.org/bpel/sample" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <response>2</response>
</tns:TriangleResponse>
</bpel:literal></bpel:from>
                                            <bpel:to part="payload" variable="output">
                                            </bpel:to>
                                        </bpel:copy>
                                        
                                        <bpel:copy>
                                            <bpel:from expressionLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                                                <![CDATA[2]]>
                                            </bpel:from>
                                            <bpel:to part="payload" variable="output">
                                                <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                                                    <![CDATA[response]]>
                                                </bpel:query>
                                            </bpel:to>
                                        </bpel:copy>
                                    </bpel:assign>
                                </bpel:sequence>
                            </bpel:elseif><bpel:else>
                                <bpel:sequence name="Scalene">
                                    <bpel:assign validate="no" name="AssignScalene">
                                        <bpel:copy>
                                            <bpel:from><bpel:literal xml:space="preserve"><tns:TriangleResponse xmlns:tns="http://eclipse.org/bpel/sample" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <response>1</response>
</tns:TriangleResponse>
</bpel:literal></bpel:from>
                                            <bpel:to part="payload" variable="output">
                                            </bpel:to>
                                        </bpel:copy>
                                        <bpel:copy>
                                            <bpel:from expressionLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                                                
                                                <![CDATA[1]]>
                                            </bpel:from>
                                            <bpel:to part="payload" variable="output">
                                                <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                                                    <![CDATA[response]]>
                                                </bpel:query>
                                            </bpel:to>
                                        </bpel:copy>
                                    </bpel:assign>
                                </bpel:sequence>
                            </bpel:else></bpel:if>
                    </bpel:sequence>
                    <bpel:else>
                        <bpel:sequence name="EndExecution2">
                            <bpel:assign validate="no" name="AddNoFormable">
                                <bpel:copy>
                                    <bpel:from><bpel:literal xml:space="preserve"><tns:TriangleResponse xmlns:tns="http://eclipse.org/bpel/sample" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <response>-2</response>
</tns:TriangleResponse>
</bpel:literal></bpel:from>
                                    <bpel:to part="payload" variable="output">
                                    </bpel:to>
                                </bpel:copy>
                                <bpel:copy>
                                    <bpel:from expressionLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                                        
                                        <![CDATA[-2]]>
                                    </bpel:from>
                                    <bpel:to part="payload" variable="output">
                                        <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                                            <![CDATA[response]]>
                                        </bpel:query>
                                    </bpel:to>
                                </bpel:copy>
                            </bpel:assign>
                        </bpel:sequence>
                    </bpel:else>
                </bpel:if>
            </bpel:sequence>
            <bpel:else>
                <bpel:sequence name="EndExecution">
                    <bpel:assign validate="no" name="AssignNoTriangle">
                        <bpel:copy>
                            <bpel:from><bpel:literal xml:space="preserve"><tns:TriangleResponse xmlns:tns="http://eclipse.org/bpel/sample" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <response>-1</response>
</tns:TriangleResponse>
</bpel:literal></bpel:from>
                            <bpel:to part="payload" variable="output">
                            </bpel:to>
                        </bpel:copy>
                        
                        <bpel:copy>
                            <bpel:from expressionLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0">
                                
                                <![CDATA[-1]]>
                            </bpel:from>
                            <bpel:to part="payload" variable="output">
                                <bpel:query queryLanguage="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0"><![CDATA[response]]></bpel:query>
                            </bpel:to>
                        </bpel:copy>
                    </bpel:assign>
                </bpel:sequence>
            </bpel:else>
        </bpel:if>
        <bpel:reply name="replyOutput" partnerLink="client" operation="process" portType="tns:Triangle" variable="output">
        </bpel:reply>
    </bpel:sequence>    
</bpel:process>

