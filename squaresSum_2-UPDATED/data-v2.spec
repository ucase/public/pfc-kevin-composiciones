typedef int ( min = 0, max = 4294967295 ) TSquaresSumRequest; 
typedef int ( min = 0, max = 4294967295 ) TSquaresSumResponse; 
typedef int ( min = 0, max = 4294967295 ) TSquaresSumCount; 
typedef int ( min = 0, max = 4294967295 ) TSquaresSumSum; 
typedef float ( min = 0, max = 4294967295 ) TSquaresSumVariance; 
typedef int ( min = 0, max = 4294967295 ) TSquaresSumCountVariance; 
typedef float ( min = 0, max = 4294967295 ) TSquaresSumSumVariance; 

TSquaresSumRequest n;
TSquaresSumResponse response;
TSquaresSumCount count;
TSquaresSumSum sum;
TSquaresSumVariance variance;
TSquaresSumCountVariance countvariance;
TSquaresSumSumVariance sumvariance;
