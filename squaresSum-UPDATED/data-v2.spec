typedef int ( min = 0, max = 4294967295 ) TSquaresSumRequest; 
typedef int ( min = 0, max = 4294967295 ) TSquaresSumResponse; 
typedef int ( min = 0, max = 4294967295 ) TSquaresSumCount; 
typedef int ( min = 0, max = 4294967295 ) TSquaresSumSum; 

TSquaresSumRequest n;
TSquaresSumResponse response;
TSquaresSumCount count;
TSquaresSumSum sum;
