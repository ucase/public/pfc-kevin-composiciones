<!--
   Copyright (C) 2012-2015 Alvaro Cortijo-García, Antonio García-Domínguez

   This file is part of the SquaresSum composition in the UCASE
   WS-BPEL composition repository.

   This program is free software: you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<stylesheet xmlns="http://www.w3.org/1999/XSL/Transform" xmlns:p="http://xml.netbeans.org/schema/squaresSum">

	<param name="newItem"/>

	<template match="p:elements">
		<copy>
			<copy-of select="p:element"/>
			<copy-of select="$newItem"/>
		</copy>
	</template>

</stylesheet>
